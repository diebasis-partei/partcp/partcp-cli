# ParTCP CLI

A command-line client for the ParTCP server written in PHP.


# System requirements

- PHP 7 (CLI) with OpenSSL support and sodium extension


# Installation

1. **Clone git repository to your local disk.**

	````sh
	git clone https://gitlab.com/diebasis-partei/partcp/partcp-cli.git
	````

2. **Put partcp script file in your $PATH.**

	Create a symbolic link to the partcp script file in a directory which is 
	listed in your $PATH environment variable. For example:

	````sh
	ln -s partcp-cli/partcp ~/bin
	````


# Usage

Type `partcp help` to get usage hints.


# Acknowledgements

This script includes the [Spyc PHP class](https://github.com/mustangostang/spyc) 
for parsing and dumping YAML code.

